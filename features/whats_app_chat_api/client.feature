Feature: Whatts App client class

  Scenario Outline: A client retrieving authenticated status
    Given we have a whats_app_chat_api.Client instance
    When the api status is <status>
    Then the whats_app_chat_api.Client.get_status results <status>

    Examples:
      |  status          |
      |  init            |
      |  loading         |
      |  got qr code     |
      |  authenticated   |

  Scenario: A client successfully send a message to phone
    Given we have a whats_app_chat_api.Client instance
    When we successfully send a message to phone
    Then the send_message_to_phone return must contain sent equal True

  Scenario: A client unsuccessfully send a message to phone
    Given we have a whats_app_chat_api.Client instance
    When we unsuccessfully send a message to phone
    Then the send_message_to_phone return must contain sent equal False

#!/usr/bin/env python

from distutils.core import setup

REQUIREMENTS_FILE = 'requirements.txt'
requirements = []

with open(REQUIREMENTS_FILE) as req_file:
    requirements = req_file.readlines()

setup(
    name='whats_app_chat_api',
    version='0.4.2',
    description='Api implementation for https://chat-api.com service',
    author='Jean Carlos Sales Pantoja',
    author_email='je.cspantoja@gmail.com',
    url='https://gitlab.com/je-cspantoja/whats_app_chat_api-python',
    packages=[
        'whats_app_chat_api',
        'whats_app_chat_api.resources'
    ],
    install_requires=requirements
)

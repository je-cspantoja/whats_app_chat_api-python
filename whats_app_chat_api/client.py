import requests

from .resources import StatusResource
from .resources import SendMessageResource
from .resources import SendFileResource

class Client(object):

    def __init__(self, api_url, api_token):
        connection = requests.Session()
        connection.params = {
            'token': api_token
        }

        self.status_resource = StatusResource(
            connection=connection,
            api_url=api_url
        )

        self.send_message_resource = SendMessageResource(
            connection=connection,
            api_url=api_url
        )

        self.send_file_resource = SendFileResource(
            connection=connection,
            api_url=api_url
        )

    def get_status(self):
        return self.status_resource.get_status()

    def send_message_to_phone(self, phone, body):
        return self.send_message_resource.send_to_phone(
            phone=phone,
            body=body
        )

    def send_message_to_chat(self, chat_id, body):
        return self.send_message_resource.send_to_chat(
            chat_id=chat_id,
            body=body
        )

    def send_file_to_phone(self, phone, body, filename):
        return self.send_file_resource.send_to_phone(
            phone=phone,
            body=body,
            filename=filename
        )

    def send_file_to_chat(self, chat_id, body, filename):
        return self.send_file_resource.send_to_chat(
            chat_id=chat_id,
            body=body,
            filename=filename
        )

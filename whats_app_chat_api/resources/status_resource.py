from .resource import Resource

class StatusResource(Resource):
    RESOURCE_NAME = "status"

    def __init__(self, connection, api_url):
        super().__init__(connection, api_url, self.RESOURCE_NAME)

    def get_status(self):
        response = self._get()
        return GetStatusResponse(response)

class GetStatusResponse(object):
    ACCOUNT_STATUS_INIT = "ini"
    ACCOUNT_STATUS_LOADING = "loading"
    ACCOUNT_STATUS_GOT_QR_CODE = "got qr code"
    ACCOUNT_STATUS_AUTHENTICATED= "authenticated"

    def __init__(self, response):
        self._response = response

    def account_status_equals(self, account_status):
        return self.account_status == account_status
    @property
    def account_status(self):
        return self._response.get('accountStatus')

    @property
    def qr_code(self):
        return self._response.get('qrCode')

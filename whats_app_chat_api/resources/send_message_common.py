class SendMessageResponse(object):

    def __init__(self, response):
        self._response = response

    @property
    def sent(self):
        return self._response.get('sent')

    @property
    def message(self):
        return self._response.get('message')

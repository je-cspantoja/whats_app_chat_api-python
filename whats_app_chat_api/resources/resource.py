class Resource(object):

    def __init__(self, connection, api_url, resource):
        self.connection = connection
        self.api_url = api_url
        self.resource = resource

    def _build_url(self):
        rurl = ResourceURL(self.api_url, self.resource)
        return str(rurl)

    def _get(self, params={}):
        response = self.connection.get(
            self._build_url(), params=params
        )

        return response.json()

    def _post(self, json, params={}):
        response = self.connection.post(
            self._build_url(), json=json, params=params
        )

        return response.json()

class ResourceURL(object):
    URL_FORMAT = "{api_url}/{resource}"

    def __init__(self, api_url, resource):
        api_url = api_url.strip()
        resource = resource.strip()

        if api_url.endswith("/"):
            api_url = api_url[:-1]

        if resource.startswith("/"):
            resource = resource[1:]

        self.api_url = api_url
        self.resource = resource

    def __str__(self):
        return self.URL_FORMAT.format(
            api_url=self.api_url,
            resource=self.resource
        )

from .send_message_common import SendMessageResponse
from .resource import Resource

class SendFileResource(Resource):
    RESOURCE_NAME = "sendFile"

    def __init__(self, connection, api_url):
        super().__init__(connection, api_url, self.RESOURCE_NAME)

    def send_to_phone(self, phone, body, filename):
        """
        :type phone: str
        :type body: str
        """

        json = {
            'phone': phone,
            'body': body,
            'filename': filename
        }

        response = self._post(json=json)
        return SendMessageResponse(response)

    def send_to_chat(self, chat_id, body, filename):
        """
        :type chat_id: str
        :type body: str
        """

        json = {
            'chatId': chat_id,
            'body': body,
            'filename': filename
        }

        response = self._post(json=json)
        return SendMessageResponse(response)

from .send_message_common import SendMessageResponse
from .resource import Resource

class SendMessageResource(Resource):
    RESOURCE_NAME = "sendMessage"

    def __init__(self, connection, api_url):
        super().__init__(connection, api_url, self.RESOURCE_NAME)

    def send_to_phone(self, phone, body):
        """
        :type phone: str
        :type body: str
        """

        json = {
            'phone': phone,
            'body': body
        }

        response = self._post(json=json)
        return SendMessageResponse(response)

    def send_to_chat(self, chat_id, body):
        """
        :type chat_id: str
        :type body: str
        """

        json = {
            'chatId': chat_id,
            'body': body
        }

        response = self._post(json=json)
        return SendMessageResponse(response)

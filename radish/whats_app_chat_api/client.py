from radish import given, when, then
import requests_mock

from whats_app_chat_api import Client

API_URL = "http://test.com"
API_TOKEN = "12345"

@given("we have a whats_app_chat_api.Client instance")
def step_given(step):
    step.context.client = Client(API_URL, API_TOKEN)

@when("the api status is {status}")
def step_when(step, status):
    with requests_mock.Mocker() as m:

        response = {
            'accountStatus': status
        }

        url = 'http://test.com/status'
        m.get(url, json=response)
        status = step.context.client.get_status()
        step.context.result = status

@then("the whats_app_chat_api.Client.get_status results {status}")
def step_then(step, status):
    assert step.context.result.account_status_equals(status)

@when("we successfully send a message to phone")
def succesfully_send_message_to_phone(step):
    with requests_mock.Mocker() as m:

        response = {
            'sent': True,
            'message': ''
        }

        url = 'http://test.com/sendMessage'
        m.post(url, json=response)
        result = step.context.client.send_message_to_phone("48999999999", "some message")
        step.context.result = result

@then("the send_message_to_phone return must contain sent equal True")
def succesfully_send_message_to_phone_return_must_contain_sent_equal_true(step):
    assert step.context.result.sent == True

@when("we unsuccessfully send a message to phone")
def unsuccesfully_send_message_to_phone(step):
    with requests_mock.Mocker() as m:

        response = {
            'sent': False,
            'message': ''
        }

        url = 'http://test.com/sendMessage'
        m.post(url, json=response)
        result = step.context.client.send_message_to_phone("48999999999", "some message")
        step.context.result = result

@then("the send_message_to_phone return must contain sent equal False")
def unsuccesfully_send_message_to_phone_return_must_contain_sent_equal_false(step):
    assert step.context.result.sent == False
